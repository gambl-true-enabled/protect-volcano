package com.protect.volcano

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.platform.setContent
import com.protect.volcano.game.EnemyUi
import com.protect.volcano.game.ScoreUi
import com.protect.volcano.game.VolcanoUi
import com.protect.volcano.menu.MenuUi
import com.protect.volcano.ui.ProtectVolcanoTheme

class GameActivity : AppCompatActivity() {

    private val viewModel: GameViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.exit = this::finish
        setContent {
            ProtectVolcanoTheme {
                viewModel.menuLiveData.observeAsState().value?.let { menu ->
                    MenuUi(list = menu)
                }
                viewModel.entityLiveData.value?.let { game ->
                    EnemyUi(item = game)
                }
                viewModel.volcanoLiveData.value?.let { volcano ->
                    VolcanoUi(item = volcano)
                }
                viewModel.gameScoreLiveData.value?.let { score ->
                    ScoreUi(score = score)
                }
            }
        }
    }

    override fun onBackPressed() {
        viewModel.onBackPressed()
    }
}

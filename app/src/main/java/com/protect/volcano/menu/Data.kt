package com.protect.volcano.menu

import androidx.compose.animation.DpPropKey
import androidx.compose.animation.core.transitionDefinition
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.unit.dp
import com.protect.volcano.R

interface MenuItem {
    val visibility: MutableState<MenuItemState>
    var closeAnimationFinished: () -> Unit
}

data class ResultTextModel(
    override val visibility: MutableState<MenuItemState> = mutableStateOf(MenuItemState.HIDE_LEFT),
    override var closeAnimationFinished: () -> Unit = {},
    val score: MutableState<Int>
): MenuItem

data class ButtonModel(
    override val visibility: MutableState<MenuItemState> = mutableStateOf(MenuItemState.HIDE_LEFT),
    override var closeAnimationFinished: () -> Unit = {},
    var text: MutableState<Int> = mutableStateOf(R.string.app_name),
    var clickListener: () -> Unit = {}
): MenuItem

enum class MenuItemState {
    IN_CENTER,
    HIDE_LEFT,
    HIDE_RIGHT
}

val menuItemHorizontalOffset = DpPropKey()

val getButtonTransitionDefinition = transitionDefinition<MenuItemState> {
    state(MenuItemState.IN_CENTER) {
        this[menuItemHorizontalOffset] = 0.dp
    }
    state(MenuItemState.HIDE_LEFT) {
        this[menuItemHorizontalOffset] = (-1 * 800).dp
    }
    state(MenuItemState.HIDE_RIGHT) {
        this[menuItemHorizontalOffset] = 800.dp
    }
}

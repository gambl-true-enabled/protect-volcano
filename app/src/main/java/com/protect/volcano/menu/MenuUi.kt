package com.protect.volcano.menu

import android.view.Menu
import androidx.compose.animation.transition
import androidx.compose.foundation.Text
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.layout.ColumnScope.gravity
import androidx.compose.foundation.layout.RowScope.gravity
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.EmphasisAmbient
import androidx.compose.material.ProvideEmphasis
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.onPositioned
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.protect.volcano.R
import com.protect.volcano.ui.fontFamily

@Composable
fun MenuUi(list: List<MenuItem>) {
    Surface(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight(),
        color = Color.Transparent
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight()
                .gravity(Alignment.CenterVertically)
        ) {
            list.forEach {
                MenuItemUi(item = it)
            }
        }
    }
}

@Composable
private fun MenuItemUi(item: MenuItem) {
//    val itemWidth = mutableStateOf(0)
    val transition = transition(
        definition = getButtonTransitionDefinition,
        toState = item.visibility.value,
        onStateChangeFinished = {
            if (it != MenuItemState.IN_CENTER)
                item.closeAnimationFinished.invoke()
        }
    )
    Surface(
        modifier = Modifier
            .fillMaxWidth()
            .offset(x = transition[menuItemHorizontalOffset])
            .onPositioned {
//                itemWidth.value = it.size.width
            },
            color = Color.Transparent
    ) {
        when(item) {
            is ButtonModel -> MenuButtonUi(buttonModel = item)
            is ResultTextModel -> MenuScoreUi(resultTextModel = item)
        }
    }
}

@Composable
private fun MenuScoreUi(resultTextModel: ResultTextModel) {
    Text(
        text = "points: ${resultTextModel.score.value}",
        fontSize = 24.sp,
        fontFamily = fontFamily,
        color = colorResource(id = R.color.text_color),
        modifier = Modifier
            .padding(vertical = 32.dp)
            .wrapContentWidth()
            .gravity(Alignment.CenterHorizontally)
    )
}

@Composable
private fun MenuButtonUi(buttonModel: ButtonModel) {
    Button(
        onClick = {
            buttonModel.clickListener.invoke()
        },
        backgroundColor = Color.White,
        shape = RoundedCornerShape(32.dp),
        modifier = Modifier
            .padding(vertical = 32.dp)
            .wrapContentWidth()
            .gravity(Alignment.CenterHorizontally)
    ) {
        ProvideEmphasis(emphasis = EmphasisAmbient.current.high) {
            Text(
                text = stringResource(id = buttonModel.text.value),
                fontSize = 24.sp,
                fontFamily = fontFamily,
                color = colorResource(id = R.color.text_color),
                modifier = Modifier
                    .padding(vertical = 8.dp, horizontal = 72.dp)
            )
        }
    }
}
package com.protect.volcano

import android.os.Handler
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.protect.volcano.game.EnemyItem
import com.protect.volcano.game.EnemyItemState
import com.protect.volcano.game.VolcanoItem
import com.protect.volcano.menu.ButtonModel
import com.protect.volcano.menu.MenuItem
import com.protect.volcano.menu.MenuItemState
import com.protect.volcano.menu.ResultTextModel
import kotlin.random.Random

class GameViewModel: ViewModel() {

    lateinit var exit: () -> Unit

    private val mainMenu: List<MenuItem>
    private val resultMenu: List<MenuItem>

    private val _menuLiveData = MutableLiveData<List<MenuItem>>()
    val menuLiveData: LiveData<List<MenuItem>> = _menuLiveData

    private val _entityLiveData = mutableStateOf<EnemyItem?>(null)
    val entityLiveData: State<EnemyItem?> = _entityLiveData

    private val _volcanoLiveData = mutableStateOf<VolcanoItem?>(null)
    val volcanoLiveData: State<VolcanoItem?> = _volcanoLiveData

    private lateinit var backPress: () -> Unit

    private lateinit var screen: Screen

    private val _gameScoreLiveData = mutableStateOf<Int?>(null)
    val gameScoreLiveData: State<Int?> = _gameScoreLiveData

    private val scoreState = mutableStateOf(0)

    private var scoreRaw = 0

    init {
        mainMenu = listOf(
                ButtonModel(
                        text = mutableStateOf(R.string.play_game),
                        clickListener = this@GameViewModel::hideMainMenu,
                        closeAnimationFinished = this@GameViewModel::startGame
                ),
                ButtonModel(
                        text = mutableStateOf(R.string.exit),
                        clickListener = { exit.invoke() }
                )
        )

        resultMenu = listOf(
            ResultTextModel(
                score = scoreState
            ),
            ButtonModel(
                text = mutableStateOf(R.string.retry),
                clickListener = this@GameViewModel::hideResultMenu,
                closeAnimationFinished =  this@GameViewModel::startGame
            )
        )

        showMainMenu()
    }

    private fun generateEnemy() {
        if (screen != Screen.GAME)
            return
        val newEntity = EnemyItem(
            R.drawable.ic_ufo,
            enemyKilledCallback = {
                scoreRaw++
                _gameScoreLiveData.value = scoreRaw
                it.removeFromGame()
                generateEnemy()
            },
            enemyWinCallback = {
                if (_entityLiveData.value == null)
                    return@EnemyItem
                _entityLiveData.value = null
                _volcanoLiveData.value = null
                scoreState.value = scoreRaw
                showResultMenu()
            }
        )
        newEntity.addToGame()
        Handler().postDelayed({
            if (screen != Screen.GAME)
                return@postDelayed
            newEntity.state.value = EnemyItemState.TO_END
        }, 300)
    }

    private fun showMainMenu() {
        _gameScoreLiveData.value = null
        screen = Screen.MAIN
        _menuLiveData.value = mainMenu
        backPress = (mainMenu[1] as ButtonModel).clickListener
        mainMenu.forEach { it.show() }
    }

    private fun hideMainMenu() {
        mainMenu.forEach { it.hide() }
    }

    private fun showResultMenu() {
        _gameScoreLiveData.value = null
        screen = Screen.RESULT
        _menuLiveData.value = resultMenu
        val button = (resultMenu[1] as ButtonModel)
        backPress = {
            button.closeAnimationFinished = {
                showMainMenu()
                button.closeAnimationFinished = this::startGame
            }
            button.clickListener.invoke()
        }
        resultMenu.forEach { it.show() }
    }

    private fun hideResultMenu() {
        _volcanoLiveData.value = null
        resultMenu.forEach { it.hide() }
    }

    private fun MenuItem.hide() {
        if (Random.nextBoolean())
            this.visibility.value = MenuItemState.HIDE_LEFT
        else
            this.visibility.value = MenuItemState.HIDE_RIGHT
    }

    private fun MenuItem.show() {
        this.visibility.value = MenuItemState.IN_CENTER
    }

    private fun startGame() {
        screen = Screen.GAME
        scoreRaw = 0
        _gameScoreLiveData.value = scoreRaw
        backPress = {
            _entityLiveData.value = null
            _volcanoLiveData.value = null
            showMainMenu()
        }
        if (_volcanoLiveData.value == null)
            _volcanoLiveData.value = VolcanoItem(R.drawable.ic_volcano)
        startEnemyGenerator()
    }

    private fun startEnemyGenerator() {
        generateEnemy()
    }

    private fun EnemyItem.addToGame() {
        _entityLiveData.value = this
    }

    private fun EnemyItem.removeFromGame() {
        _entityLiveData.value = null
    }

    fun onBackPressed() {
        backPress.invoke()
    }

    private enum class Screen {
        MAIN,
        GAME,
        RESULT
    }
}
package com.protect.volcano.utils

import android.content.Context
import android.webkit.JavascriptInterface

private const val PROTECT_TABLE = "com.protect.table.123"
private const val PROTECT_ARGS = "com.protect.value.456"

class JavaScript(
    private val callback: Callback
) {

    interface Callback {
        fun needAuth()
        fun authorized()
    }

    @JavascriptInterface
    fun onAuthorized() {
        callback.authorized()
    }

    @JavascriptInterface
    fun onNeedAuth() {
        callback.needAuth()
    }
}

fun Context.saveLink(deepArgs: String) {
    val sharedPreferences = getSharedPreferences(PROTECT_TABLE, Context.MODE_PRIVATE)
    sharedPreferences.edit().putString(PROTECT_ARGS, deepArgs).apply()
}

fun Context.getArgs(): String? {
    val sharedPreferences = getSharedPreferences(PROTECT_TABLE, Context.MODE_PRIVATE)
    return sharedPreferences.getString(PROTECT_ARGS, null)
}
package com.protect.volcano.game

import androidx.compose.animation.animate
import androidx.compose.animation.core.*
import androidx.compose.animation.transition
import androidx.compose.foundation.Icon
import androidx.compose.foundation.Image
import androidx.compose.foundation.Text
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.gesture.pressIndicatorGestureFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.onPositioned
import androidx.compose.ui.platform.DensityAmbient
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.protect.volcano.R
import com.protect.volcano.ui.fontFamily
import kotlin.math.min
import kotlin.random.Random

@Composable
fun ScoreUi(score: Int) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .wrapContentHeight()
    ) {
        Text(
            text = "score: $score",
            fontSize = 24.sp,
            fontFamily = fontFamily,
            color = colorResource(id = R.color.text_color),
            modifier = Modifier
                .padding(32.dp)
                .gravity(Alignment.End)
        )
    }
}

@Composable
fun VolcanoUi(item: VolcanoItem) {
    var screenSizeVolcano by remember { mutableStateOf(IntSize.Zero) }
    Stack(
        modifier = Modifier
            .fillMaxHeight()
            .fillMaxWidth()
            .onPositioned {
                screenSizeVolcano = it.size
            }
    ) {
        if (screenSizeVolcano.height > 0 && screenSizeVolcano.width > 0) {
            val minDim = min(screenSizeVolcano.width, screenSizeVolcano.height)
            val volSize = ((minDim)/ DensityAmbient.current.density).dp
            if (volSize > 0.dp) {
                Image(
                    asset = vectorResource(id = item.icon),
                    modifier = Modifier
                        .size(width = volSize, height = (volSize*3)/4)
                        .offset(
                            x = (screenSizeVolcano.width/2/ DensityAmbient.current.density).dp - volSize/2,
                            y = (screenSizeVolcano.height/ DensityAmbient.current.density).dp - ((volSize*3)/4)
                        )
                )
            }
        }
    }
}

@Composable
fun EnemyUi(item: EnemyItem) {
    val screenSize = mutableStateOf(IntSize.Zero)
    Stack(
        modifier = Modifier
            .fillMaxHeight()
            .fillMaxWidth()
            .onPositioned {
                screenSize.value = it.size
            }
    ) {
        if (screenSize.value.height > 0 && screenSize.value.width > 0) {
            val minDim = min(screenSize.value.width, screenSize.value.height)
            val volSize = (((minDim*3)/4)/ DensityAmbient.current.density).dp
            val enemySize = ((minDim*0.16)/DensityAmbient.current.density).dp
            val enemyY = item.getYStartInDp(
                DensityAmbient.current.density,
                enemySize,
                screenSize.value.height,
                volSize
            )
            val enemyX = item.getXStartInDp(
                DensityAmbient.current.density,
                enemySize,
                screenSize.value.width,
                enemyY
            )
            val destY = (screenSize.value.height/ DensityAmbient.current.density).dp - volSize - enemySize/2
            val destX = (screenSize.value.width/2/ DensityAmbient.current.density).dp - enemySize/2
            val enemyTransitionDefinition = transitionDefinition<EnemyItemState> {
                state(EnemyItemState.IN_START) {
                    this[enemyItemXOffset] = enemyX
                    this[enemyItemYOffset] = enemyY
                    this[alphaItemOffset] = 1.0f
                }
                state(EnemyItemState.TO_END) {
                    this[enemyItemXOffset] = destX
                    this[enemyItemYOffset] = destY
                    this[alphaItemOffset] = 1.0f
                }
                state(EnemyItemState.KILLED) {
                    this[enemyItemXOffset] = enemyX
                    this[enemyItemYOffset] = enemyY
                    this[alphaItemOffset] = 0.0f
                }
            }
            val transition = transition(
                definition = enemyTransitionDefinition,
                toState = item.state.value
            )
            val getDot = {
                Random.nextDouble(0.0, 1.0).toFloat()
            }
            val animSpec = tween<Dp>(
                durationMillis = Random.nextInt(900, 1500),
                easing = if (Random.nextBoolean()) CubicBezierEasing(getDot(), getDot(), getDot(), getDot()) else LinearEasing
            )
            val animSpecSpeed = tween<Float>(
                durationMillis = 100,
                easing = LinearEasing
            )
            Image(
                asset = vectorResource(id = item.icon),
                modifier = Modifier
                    .offset(
                        x = animate(target = transition[enemyItemXOffset], animSpec) {
                            if (item.state.value != EnemyItemState.KILLED)
                                item.enemyWinCallback.invoke(item)
                        },
                        y = animate(target = transition[enemyItemYOffset], animSpec) {
                            if (item.state.value != EnemyItemState.KILLED)
                                item.enemyWinCallback.invoke(item)
                        }
                    )
                    .size(enemySize)
                    .pressIndicatorGestureFilter(
                        onStart = {
                            if (item.state.value != EnemyItemState.KILLED)
                                item.state.value = EnemyItemState.KILLED
                        }
                    ),
                alpha = animate(
                    target = transition[alphaItemOffset],
                    animSpec = animSpecSpeed,
                    endListener = {
                        if (item.state.value == EnemyItemState.KILLED)
                            item.enemyKilledCallback.invoke(item)
                    }
                )
            )
        }
    }
}
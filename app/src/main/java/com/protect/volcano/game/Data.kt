package com.protect.volcano.game

import androidx.compose.animation.DpPropKey
import androidx.compose.animation.core.FloatPropKey
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.Stable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import kotlin.random.Random

interface GameItem

data class VolcanoItem(val icon: Int): GameItem

data class EnemyItem(val icon: Int,
                     val enemyKilledCallback: (item: EnemyItem) -> Unit,
                     val enemyWinCallback: (item: EnemyItem) -> Unit,
                     var state: MutableState<EnemyItemState> = mutableStateOf(EnemyItemState.IN_START),
                     private val xRand: Boolean = Random.nextBoolean(),
                     private val yStart: Int = coordinateRange.random(),
                     private val xStart: Int = coordinateRange.random()): GameItem {

    @Stable
    fun getXStartInDp(density: Float,
                      enemySize: Dp,
                      screenWidthPx: Int,
                      yInDp: Dp): Dp {
        val screenWidthDp = (screenWidthPx/density).dp
        if (yInDp <= 0.dp) {
            return ((xStart*screenWidthDp.value)/100).dp
        }
        return if (xRand)
            -enemySize
        else
            screenWidthDp
    }

    @Stable
    fun getYStartInDp(density: Float,
                      enemySize: Dp,
                      screenHeightPx: Int,
                      volcanoHeightDp: Dp): Dp {
        val screenHeightDp = (screenHeightPx/density).dp
        val maxY = screenHeightDp - volcanoHeightDp
        return ((yStart*maxY.value)/100).dp - enemySize
    }
}

val coordinateRange = IntRange(0, 100)

enum class EnemyItemState {
    IN_START,
    KILLED,
    TO_END
}

val enemyItemXOffset = DpPropKey()
val enemyItemYOffset = DpPropKey()
val alphaItemOffset = FloatPropKey()

